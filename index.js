const Subdomain = require('koa-subdomain');

/**
 *
 * @param {string} subdomain
 * @param {Application.Middleware} mw
 * @return {Application.Middleware}
 */
module.exports = function (subdomain, mw) {
    return new Subdomain().use(subdomain, mw).routes();
};